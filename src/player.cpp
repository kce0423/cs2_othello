#include "player.h"
#include <vector>
#include <sys/time.h>
#include <climits>
#include <algorithm>

#define MIN_HEURISTIC (INT_MIN)
#define MAX_HEURISTIC (INT_MAX)

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    this->board = new Board();
    this->my_color = side;
    this->opp_color = (side == WHITE) ? BLACK : WHITE;
    if (side == BLACK)
        cerr << "player is BLACK" << endl;
    else
        cerr << "player is WHITE" << endl;
}

/*
 * Destructor for the player.
 */
Player::~Player() {
    delete board;
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    // Updates opponent's move
    board->doMove(opponentsMove, opp_color);

    // Check if I have a move
    if (!board->hasMoves(my_color))
        return NULL;

    // Initialize max heuristics
    int max_heuristic = MIN_HEURISTIC;
    int x = 0, y = 0;

    // Find valid moves
    vector<int> next;
    board->NextMoves(my_color, &next);
    for (unsigned int i = 0; i < next.size(); i++) {
        int res_x = next[i] % SIZE;
        int res_y = next[i] / SIZE;
        Move move(res_x, res_y);
        int score = heuristicMinimax(&move);
        //int score = heuristicAB(&move, board, 1,
        //                        MIN_HEURISTIC,
        //                        MAX_HEURISTIC,
        //                        opp_color);
        if (max_heuristic < score)
        {
            max_heuristic = score;
            x = res_x;
            y = res_y;
        }
    }
    cerr << "I picked (" << x << ", " << y << ")" << endl;
    Move *nextMove = new Move(x, y);

    //Update my move
    board->doMove(nextMove, my_color);

    return nextMove;
}

/*
 * Set a new initial board for the player.
 * This is for testminimax.
 */
void Player::setBoard(Board *newBoard)
{
    board = newBoard;
}

bool Player::isCorner(Move *m) {
    return (m->getX() == 0 && m->getY() == 0) ||
           (m->getX() == 0 && m->getY() == 7) ||
           (m->getX() == 7 && m->getY() == 0) ||
           (m->getX() == 7 && m->getY() == 7);
}

bool Player::isEdge(Move *m) {
    return (m->getX() == 0 || m->getX() == 7 ||
            m->getY() == 0 || m->getY() == 7);
}

bool Player::isAdjCorner(Move *m) {
    return (m->getX() == 0 && (m->getY() == 1 || m->getY() == 6)) ||
           (m->getX() == 1 && (m->getY() == 0 || m->getY() == 1 ||
                               m->getY() == 6 || m->getY() == 7)) ||
           (m->getX() == 6 && (m->getY() == 0 || m->getY() == 1 ||
                               m->getY() == 6 || m->getY() == 7)) ||
           (m->getX() == 7 && (m->getY() == 1 || m->getY() == 6));
}

/*
 * A simple heuristic that gives positive weights to
 * cells on the corner or the edge and negative weights
 * to cells adjacent to the corner.
 */
int Player::heuristicSimple(int i, int j)
{
    Board *temp = board->copy();
    Move move(i, j);
    temp->doMove(&move, my_color);

    int score = temp->count(my_color) - temp->count(opp_color);
    if (isCorner(&move))
        score *= 3;
    else if (isAdjCorner(&move))
        score *= -3;
    else if (isEdge(&move))
        score *= 2;

    delete temp;
    return score;
}


/*
 * A simple minimax algorithm.
 */
int Player::heuristicMinimax(Move *move)
{
    Board *temp = board->copy();
    temp->doMove(move, my_color);
    int min_heuristic = 64;

    vector<int> next;
    temp->NextMoves(opp_color, &next);
    for (unsigned int i = 0; i < next.size(); i++) {
        Move move2(next[i] % 8, next[i] / 8);
        Board *temp2 = temp->copy();
        temp2->doMove(&move2, opp_color);
        int heuristic = temp2->countMobility(my_color) - temp2->countMobility(opp_color);
        min_heuristic = (min_heuristic > heuristic) ? heuristic : min_heuristic;
        delete temp2;
    }

    delete temp;
    return min_heuristic;
}

/*
 * AB pruning algorithm
 */
int Player::heuristicAB(Move *move, Board *board, int depth, int alpha, int beta, Side side)
{
    /* Make a move */
    Board *temp = board->copy();
    temp->doMove(move, side);
    vector<int> next;
    Side opp_side = (side == BLACK) ? WHITE : BLACK;

    /* Return heuristic in the deepest level. */
    if (depth == 0) {
        delete temp;
        return temp->countMobility(my_color) - temp->countMobility(opp_color);
    }
    else { 
       temp->NextMoves(opp_side, &next);
       cerr << "size = " << next.size() << endl;
       if (next.size() == 0) {
           delete temp;
           return temp->countMobility(my_color) - temp->countMobility(opp_color);
       }

        /* If my turn, maximize alpha */
        if (side == my_color) {
            for (unsigned int i = 0; i < next.size(); i++) {
                Move move2(next[i] % 8, next[i] / 8);
                int next_beta = heuristicAB(&move2, temp, depth-1, alpha, beta, opp_color);
                alpha = max(alpha, next_beta);
                if (beta <= alpha)
                    break;
            }
            delete temp;
            return alpha;
        }
        /* If opponent's turn, minimize beta */
        else {
            for (unsigned int i = 0; i < next.size(); i++) {
                Move move2(next[i] % 8, next[i] / 8);
                int next_alpha = heuristicAB(&move2, temp, depth-1, alpha, beta, opp_color);
                beta = min(beta, next_alpha);
                if (beta <= alpha)
                    break;
            }
            delete temp;
            return beta;
        }
    }
}
