This is about Othello AI competition for CS2 assignment

Team Members: Chung Eun Kim / Chan-Hee Koh
Assignment 9  commit : c9d39a177a935f89bfb24a122ee89b0ac1893d9a
Assignment 10 commit : 0c6c78a492dfda0a523ac0068c984a16cdb91c42

Directory hierarchy:
 Binaries, object files and source codes are organized into subdirectories of 
 bin, obj and src, respectively.

Usage:
 Our executable is named as "ck_squared". Thus, instead of using just player,
 you need to type in "ck_squared".

 ex) ./bin/testgame [BLACK] [WHITE]
 Here the player is black. 
 ex) ./bin/testgame ck_squared SimplePlayer 
 Here the player is white
 ex) ./bin/testgame SimplePlayer ck_squared


Testing:
 There is bash file that characterizes the performances of the SimplePlayer,
 ConstantTimePlayer, and the BetterPlayer algorithm by running testgame 10 times 
 and saving the winners to a textfile. The perf_test.cpp opens and calculates 
 the performance and outputs it to stdout.

Usage: 
 ex) 
    >> make testing 
 	>> ./test/perf_test
