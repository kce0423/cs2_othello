#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
    void setBoard(Board *newBoard);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;

private:
    Board *board;
    Side my_color;
    Side opp_color;

    bool isCorner(Move *m);
    bool isEdge(Move *m);
    bool isAdjCorner(Move *m);
    int heuristicSimple(int i, int j);
    int heuristicMinimax(Move *move);
    int heuristicAB(Move *move, Board *board, int depth, int alpha, int beta, Side side);
};

#endif
