#!/bin/bash
# This script runs the testgame 100 times to determine its performance
# We want to make sure that we get a performance of a win-rate >= 80%.

COUNTER=0
WINS=0
while [  $COUNTER -lt 10 ]; do
	# This is the simple player implementation
	./bin/testgame ck_squared SimplePlayer 2>&1 | tail -1 >>winlog &
	
	search_terms='SimplePlayer' 
	sleep 4
	kill $(ps aux | grep "$search_terms" | grep -v 'grep' | awk '{print $2}')

	COUNTER=`expr $COUNTER + 1`

done


