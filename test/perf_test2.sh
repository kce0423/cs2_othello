#!/bin/bash
# This script runs the testgame 10 times to determine its performance


COUNTER=0
WINS=0
while [  $COUNTER -lt 10 ]; do
	# This is the Constant Time implementation
	./bin/testgame ck_squared ConstantTimePlayer 2>&1 | tail -1 >>winlog &
	
	search_terms='ConstantTimePlayer' 
	sleep 4
	kill $(ps aux | grep "$search_terms" | grep -v 'grep' | awk '{print $2}')

	COUNTER=`expr $COUNTER + 1`

done


