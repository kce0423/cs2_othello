#include <cstdio>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main()
{
	string line;
	int num_wins = 0;
	int count = 0;
	int test_id;
	double win_rate = 0;
	string cmd1 = "./test/perf_test.sh";
	string cmd2 = "./test/perf_test2.sh";
	string cmd3 = "./test/perf_test3.sh";
	string rm_file = "rm winlog";
	cout << "Please enter the number of the algorithm to be tested: " << endl;
	cout << "1: SimplePlayer \n2: ConstantPlayerTime\n3: BetterPlayer" << endl;
	cin >> test_id;
	bool cont = 1;
	while (cont)
	{
		switch(test_id)
		{
			// invoking the bash script that will run the SimplePlayer algorithm
			case 1:
				system(cmd1.c_str());
				cont = 0;
				break;
			// invoking the bash script that will run the ConstantTimePlayer
			// algorithm
			case 2:
				system(cmd2.c_str());
				cont = 0;
				break;
			// invoking the bash script that will run the BetterPlayer algorithm
			case 3:
				system(cmd3.c_str());
				cont = 0;
				break;
			default: 
				cout << "Invalid Entry; Enter a valid case: ";
				cin >> test_id;
				continue;
		}
	}
	// The bash script saves the results to a textfile so we iterate through it
	// line by line.
	ifstream winfile ("winlog");
	if (winfile.is_open())
	{
		while ( winfile >> line )
		{
			// We have set the player to be black in the bash scripts.
			count++;
			if (line == "B")
				num_wins++;
		}
	}
	winfile.close();
	// calculating the win rate
	win_rate = (double) num_wins / (double) count;
	cout <<"Win Rate: " <<  win_rate << endl;
	system(rm_file.c_str());

	return 0;
}