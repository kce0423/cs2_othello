CC          = g++
CFLAGS      = -Wall -ansi -pedantic -ggdb
ck_squared  = ck_squared

SRC = src
OBJ = obj
BIN = bin
TEST = test

all: $(ck_squared) testgame
	
$(ck_squared): $(OBJ)/player.o $(OBJ)/board.o $(OBJ)/wrapper.o
	$(CC) -o $(BIN)/$@ $^

testgame: $(OBJ)/testgame.o
	$(CC) -o $(BIN)/$@ $^

testminimax: $(OBJ)/player.o $(OBJ)/board.o $(OBJ)/testminimax.o
	$(CC) -o $(BIN)/$@ $^

$(OBJ)/%.o: $(SRC)/%.cpp
	$(CC) -c $(CFLAGS) -x c++ $< -o $@


testing:
	chmod +x $(TEST)/perf_test.sh
	chmod +x $(TEST)/perf_test2.sh
	chmod +x $(TEST)/perf_test3.sh
	g++ -c -g -Wall -std=c++0x -o $(TEST)/perf_test.o $(TEST)/perf_test.cpp 
	g++ -o $(TEST)/perf_test $(TEST)/perf_test.o

java:
	make -C java/

cleanjava:
	make -C java/ clean

clean:
	rm -f $(OBJ)/*.o $(BIN)/$(ck_squared) $(BIN)/testgame $(BIN)/testminimax
	rm -f $(TEST)/*.o $(TEST)/perf_test
	


.PHONY: java testminimax
